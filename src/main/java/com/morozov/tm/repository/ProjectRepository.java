package com.morozov.tm.repository;

import com.morozov.tm.entity.Project;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProjectRepository implements Repository<Project> {
    private Map<String, Project> projectMap = new HashMap<>();

    @Override
    public List<Project> findAll() {
        List<Project> projectList = new ArrayList<>();
        projectList.addAll(projectMap.values());
        return projectList;
    }

    @Override
    public Project findOne(String id) {
        return projectMap.get(id);
    }

    @Override
    public void persist(Project writeEntity) {
        projectMap.put(writeEntity.getId(), writeEntity);
    }

    @Override
    public void merge(String id, Project updateEntity) {
        projectMap.put(id, updateEntity);
    }

    @Override
    public void remove(String id) {
        projectMap.remove(id);
    }

    @Override
    public void removeAll() {
        projectMap.clear();
    }

}

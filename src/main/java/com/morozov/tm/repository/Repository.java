package com.morozov.tm.repository;

import java.util.List;

public interface Repository<T> {

    List<T> findAll();

    T findOne(String id);

    void persist(T writeEntity);

    void merge(String id, T updateEntity);

    void remove(String id);

    void removeAll();

}

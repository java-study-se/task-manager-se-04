package com.morozov.tm;

import com.morozov.tm.service.Bootstrap;

public class Application {
    public static void main(String[] args) {
        Bootstrap.init();
    }
}

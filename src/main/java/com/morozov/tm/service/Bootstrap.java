package com.morozov.tm.service;

import com.morozov.tm.command.CommandEnum;
import com.morozov.tm.entity.Project;
import com.morozov.tm.entity.Task;
import com.morozov.tm.exception.RepositoryEmptyException;
import com.morozov.tm.exception.StringEmptyException;
import com.morozov.tm.repository.ProjectRepository;
import com.morozov.tm.repository.TaskRepository;
import com.morozov.tm.util.ConsoleHelper;

import java.text.ParseException;
import java.util.*;

public class Bootstrap {

    private static ProjectService projectService = new ProjectService(new ProjectRepository());
    private static TaskService taskService = new TaskService(new TaskRepository());
    private Map<String, CommandEnum> commandMap;

    public Bootstrap() {
        this.commandMap = ConsoleHelper.getFullCommandMap();
    }

    public static void init() {
        Bootstrap start = new Bootstrap();
        boolean isRunning = true;
        ConsoleHelper.writeString("Вас приветствует программа Task Manager. " +
                "Наберите \"help\" для вывода списка доступных команд. ");
        while (isRunning) {
            String console = ConsoleHelper.readString();
            CommandEnum commandEnum = start.commandMap.get(console.toLowerCase());
            if (commandEnum != null) {
                switch (commandEnum) {
                    case HELP:
                        ConsoleHelper.showAllCommand();
                        break;
                    case PROJECT_CLEAR:
                        taskService.clearTaskList();
                        projectService.clearProjectList();
                        ConsoleHelper.writeString("Список проектов очищен");
                        break;
                    case PROJECT_LIST:
                        projectList();
                        break;
                    case PROJECT_CREATE:
                        createProject();
                        break;
                    case PROJECT_UPDATE:
                        updateProject();
                        break;
                    case PROJECT_REMOVE:
                        removeProject();
                        break;
                    case TASK_LIST:
                        taskList();
                        break;
                    case TASK_CLEAR:
                        taskService.clearTaskList();
                        ConsoleHelper.writeString("Список задач очищен");
                        break;
                    case TASK_CREATE:
                        createTask();
                        break;
                    case TASK_UPDATE:
                        updateTask();
                        break;
                    case TASK_REMOVE:
                        removeTask();
                        break;
                    case TASK_LIST_BY_PROJECT_ID:
                        taskListByProjectId();
                        break;
                    case EXIT:
                        isRunning = false;
                        break;
                }
            } else {
                ConsoleHelper.writeString("Команда не распознана, введите повторно. Для вызова справки введите \"help\"");
            }
        }
    }

    private static void removeTask() {
        ConsoleHelper.writeString("Введите порядковый номер задачи для удаления");
        String idDeletedTask = ConsoleHelper.readString();
        try {
            if (taskService.deleteTask(idDeletedTask)) {
                ConsoleHelper.writeString("Задача с порядковым номером " + idDeletedTask + " удален");
            }
            ConsoleHelper.writeString("Задачи с данным ID не существует");
        } catch (StringEmptyException e) {
            ConsoleHelper.writeString("Введенные поля не могут быть пустими");
        }
    }

    private static void updateTask() {
        ConsoleHelper.writeString("Введите ID задачи для изменения");
        String updateTaskID = ConsoleHelper.readString();
        ConsoleHelper.writeString("Введите новое имя задачи");
        String updateTaskName = ConsoleHelper.readString();
        ConsoleHelper.writeString("Введите новое описание задачи");
        String updateTaskDescription = ConsoleHelper.readString();
        ConsoleHelper.writeString("Введите новую дату начала задачи в формате DD.MM.YYYY");
        String startUpdateTaskDate = ConsoleHelper.readString();
        ConsoleHelper.writeString("Введите новую дату окончания задачи в формате DD.MM.YYYY");
        String endUpdateTaskDate = ConsoleHelper.readString();
        ConsoleHelper.writeString("Введите ID проекта задачи");
        String updateTaskProjectId = ConsoleHelper.readString();
        try {
            taskService.updateTask(updateTaskID, updateTaskName, updateTaskDescription, startUpdateTaskDate, endUpdateTaskDate, updateTaskProjectId);
            ConsoleHelper.writeString("Задача с порядковым номером " + updateTaskID + " обновлена");
        } catch (RepositoryEmptyException e) {
            ConsoleHelper.writeString("Список задач пуст");
        } catch (StringEmptyException e) {
            ConsoleHelper.writeString("Введенные поля не могут быть пустими");
        } catch (ParseException e) {
            ConsoleHelper.writeString("Неверный формат даты. Введите дату в формате DD.MM.YYYY");
        }
    }

    private static void createTask() {
        ConsoleHelper.writeString("Введите имя новой задачи");
        String taskName = ConsoleHelper.readString();
        ConsoleHelper.writeString("Введите описание новой задачи");
        String taskDescription = ConsoleHelper.readString();
        ConsoleHelper.writeString("Введите дату начала новой задачи в формате DD.MM.YYYY");
        String startTaskDate = ConsoleHelper.readString();
        ConsoleHelper.writeString("Введите дату окончания новой задачи в формате DD.MM.YYYY");
        String endTaskDate = ConsoleHelper.readString();
        ConsoleHelper.writeString("Введите ID проекта задачи");
        String projectId = ConsoleHelper.readString();
        try {
            Task task = taskService.addTask(taskName, taskDescription, startTaskDate, endTaskDate, projectId);
            ConsoleHelper.writeString("Добавлена задача" + task.toString());
        } catch (StringEmptyException e) {
            ConsoleHelper.writeString("Введенные поля не могут быть пустими");
        } catch (ParseException e) {
            ConsoleHelper.writeString("Неверный формат даты. Введите дату в формате DD.MM.YYYY");
        }
    }

    private static void taskList() {
        try {
            List<Task> taskList = taskService.getAllTask();
            ConsoleHelper.writeString("Список задач:");
            for (int i = 0; i < taskList.size(); i++) {
                ConsoleHelper.writeString(String.format(("%d: %s"), i, taskList.get(i).toString()));
            }
        } catch (RepositoryEmptyException e) {
            ConsoleHelper.writeString("Список задач пуст");
        }
    }

    private static void removeProject() {
        ConsoleHelper.writeString("Введите ID проекта для удаления");
        String idDeletedProject = ConsoleHelper.readString();
        ConsoleHelper.writeString("Удаление задач с ID проекта: " + idDeletedProject);
        taskService.deleteAllTaskByProjectId(idDeletedProject);
        try {
            if (projectService.deleteProject(idDeletedProject)) {
                ConsoleHelper.writeString("Проект с ID: " + idDeletedProject + " удален");
            } else {
                ConsoleHelper.writeString("Проекта с данным ID не существует");
            }
        } catch (StringEmptyException e) {
            ConsoleHelper.writeString("ID не может быт пустым");
        }
    }

    private static void updateProject() {
        ConsoleHelper.writeString("Введите ID проекта для изменения");
        String updateProjectId = ConsoleHelper.readString();
        ConsoleHelper.writeString("Введите новое имя проекта");
        String updateProjectName = ConsoleHelper.readString();
        ConsoleHelper.writeString("Введите новое описание проекта");
        String updateProjectDescription = ConsoleHelper.readString();
        ConsoleHelper.writeString("Введите новую дату начала проекта в формате DD.MM.YYYY");
        String startUpdateDate = ConsoleHelper.readString();
        ConsoleHelper.writeString("Введите новую дату окончания проекта в формате DD.MM.YYYY");
        String endUpdateDate = ConsoleHelper.readString();
        try {
            projectService.updateProject(updateProjectId, updateProjectName, updateProjectDescription, startUpdateDate, endUpdateDate);
        } catch (RepositoryEmptyException e) {
            ConsoleHelper.writeString("Список проектов пуст");
        } catch (StringEmptyException e) {
            ConsoleHelper.writeString("Введенные поля не могут быть пустими");
        } catch (ParseException e) {
            ConsoleHelper.writeString("Неверный формат даты. Введите дату в формате DD.MM.YYYY");
        }
    }

    private static void createProject() {
        ConsoleHelper.writeString("Введите имя нового проекта");
        String projectName = ConsoleHelper.readString();
        ConsoleHelper.writeString("Введите описание нового проекта");
        String projectDescription = ConsoleHelper.readString();
        ConsoleHelper.writeString("Введите дату начала нового проекта в формате DD.MM.YYYY");
        String startProjectDate = ConsoleHelper.readString();
        ConsoleHelper.writeString("Введите дату окончания нового проекта в формате DD.MM.YYYY");
        String endProjectDate = ConsoleHelper.readString();
        try {
            Project addedProject = projectService.addProject(projectName, projectDescription, startProjectDate, endProjectDate);
            ConsoleHelper.writeString("Добавлен проект: " + addedProject.toString());
        } catch (StringEmptyException e) {
            ConsoleHelper.writeString("Введенные поля не могут быть пустими");
        } catch (ParseException e) {
            ConsoleHelper.writeString("Неверный формат даты. Введите дату в формате DD.MM.YYYY");
        }
    }

    private static void projectList() {
        try {
            List<Project> projectList = projectService.getAllProject();
            ConsoleHelper.writeString("Список проектов:");
            for (int i = 0; i < projectList.size(); i++) {
                ConsoleHelper.writeString(String.format("%d: %s", i, projectList.get(i).toString()));
            }
        } catch (RepositoryEmptyException e) {
            ConsoleHelper.writeString("Список проектов пуст");
        }
    }

    private static void taskListByProjectId() {
        ConsoleHelper.writeString("Введите ID проекта");
        String projectId = ConsoleHelper.readString();
        try {
            List<Task> taskList = taskService.getAllTaskByProjectId(projectId);
            ConsoleHelper.writeString("Список задач по проекту с ID: " + projectId);
            for (int i = 0; i < taskList.size(); i++) {
                ConsoleHelper.writeString(String.format(("%d: %s"), i, taskList.get(i).toString()));
            }
        } catch (StringEmptyException e) {
            ConsoleHelper.writeString("Введенные поля не могут быть пустими");
        } catch (RepositoryEmptyException e) {
            ConsoleHelper.writeString("Список задач пуст");
        }
    }
}


package com.morozov.tm.service;

import com.morozov.tm.entity.Project;
import com.morozov.tm.exception.RepositoryEmptyException;
import com.morozov.tm.exception.StringEmptyException;
import com.morozov.tm.repository.ProjectRepository;
import com.morozov.tm.util.ConsoleHelper;
import com.morozov.tm.util.DataValidation;

import java.text.ParseException;
import java.util.List;

public class ProjectService {

    private ProjectRepository projectRepository;

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    // отображение списка всех проектов
    public List<Project> getAllProject() throws RepositoryEmptyException {
        List<Project> projectList = projectRepository.findAll();
        DataValidation.checkEmptyRepository(projectList);
        return projectList;
    }

    // добавление нового проекта
    public Project addProject(String projectName, String projectDescription, String dataStart, String dataEnd) throws StringEmptyException, ParseException {
        DataValidation.checkEmptyString(projectName, projectDescription, dataStart, dataEnd);
        Project project = new Project();
        project.setName(projectName);
        project.setDescription(projectDescription);
        project.setStartDate(ConsoleHelper.formattedData(dataStart));
        project.setEndDate(ConsoleHelper.formattedData(dataEnd));
        projectRepository.persist(project);
        return project;
    }

    // удаление проекта
    public boolean deleteProject(String id) throws StringEmptyException {
        boolean isDeleted = false;
        DataValidation.checkEmptyString(id);
        if (projectRepository.findOne(id) != null) {
            projectRepository.remove(id);
            isDeleted = true;
        }
        return isDeleted;
    }

    // обновление проекта
    public Project updateProject(String id, String projectName, String projectDescription, String dataStart, String dataEnd) throws RepositoryEmptyException, StringEmptyException, ParseException {
        DataValidation.checkEmptyRepository(projectRepository.findAll());
        DataValidation.checkEmptyString(id, projectName, projectDescription, dataStart, dataEnd);
        Project project = new Project();
        project.setId(id);
        project.setName(projectName);
        project.setDescription(projectDescription);
        project.setStartDate(ConsoleHelper.formattedData(dataStart));
        project.setEndDate(ConsoleHelper.formattedData(dataEnd));
        projectRepository.merge(id, project);
        return project;
    }

    //очистка репозитория с проектами
    public void clearProjectList() {
        projectRepository.removeAll();
    }

}

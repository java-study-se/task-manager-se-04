package com.morozov.tm.service;

import com.morozov.tm.entity.Task;
import com.morozov.tm.exception.RepositoryEmptyException;
import com.morozov.tm.exception.StringEmptyException;
import com.morozov.tm.repository.TaskRepository;
import com.morozov.tm.util.ConsoleHelper;
import com.morozov.tm.util.DataValidation;

import java.text.ParseException;
import java.util.List;


public class TaskService {

    private TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    //отображение списка всез задач
    public List<Task> getAllTask() throws RepositoryEmptyException {
        List<Task> taskList = taskRepository.findAll();
        DataValidation.checkEmptyRepository(taskList);
        return taskList;
    }

    // добавление новой задачи
    public Task addTask(String taskName, String taskDescription, String dataStart, String dataEnd, String projectId) throws StringEmptyException, ParseException {
        DataValidation.checkEmptyString(taskName, taskDescription, dataStart, dataEnd, projectId);
        Task task = new Task();
        task.setName(taskName);
        task.setDescription(taskDescription);
        task.setStartDate(ConsoleHelper.formattedData(dataStart));
        task.setEndDate(ConsoleHelper.formattedData(dataEnd));
        task.setIdProject(projectId);
        taskRepository.persist(task);
        return task;
    }

    //удаление задачи
    public boolean deleteTask(String id) throws StringEmptyException {
        boolean isDeleted = false;
        DataValidation.checkEmptyString(id);
        if (taskRepository.findOne(id) != null) {
            taskRepository.remove(id);
            isDeleted = true;
        }
        return isDeleted;
    }

    public void updateTask(String id, String name, String description, String startDate, String endDate, String projectId) throws RepositoryEmptyException, StringEmptyException, ParseException {
        DataValidation.checkEmptyRepository(taskRepository.findAll());
        DataValidation.checkEmptyString(id, name, description, startDate, endDate, projectId);
        Task task = new Task();
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        task.setStartDate(ConsoleHelper.formattedData(startDate));
        task.setEndDate(ConsoleHelper.formattedData(endDate));
        task.setIdProject(projectId);
        taskRepository.merge(id, task);
    }

    //все задачи по ID проекта
    public List<Task> getAllTaskByProjectId(String projectId) throws StringEmptyException, RepositoryEmptyException {
        DataValidation.checkEmptyString(projectId);
        DataValidation.checkEmptyRepository(taskRepository.getAllTaskByProgectId(projectId));
        return taskRepository.getAllTaskByProgectId(projectId);
    }

    //удаление всез задач по ID Проекта
    public void deleteAllTaskByProjectId(String projectId) {
        taskRepository.deleteAllTaskByProjectId(projectId);
    }

    //очистка репозитория с задачами
    public void clearTaskList() {
        taskRepository.removeAll();
    }
}

package com.morozov.tm.util;

import com.morozov.tm.command.CommandEnum;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class ConsoleHelper {

    public static final String DATA_FORMAT = "dd.MM.yyyy";

    //чтение строки с консоли
    public static String readString() {
        String enterString = "";
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        try {
            enterString = bufferedReader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return enterString;
    }

    //вывод строки в консоль
    public static void writeString(String string) {
        System.out.println(string);
    }

    //вывод всех команд и описания к ним
    public static void showAllCommand() {
        for (CommandEnum command : CommandEnum.values()) {
            System.out.println(String.format("%s: %s", command.getName(), command.getDescription()));
        }
    }

    public static Date formattedData(String data) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat(DATA_FORMAT);
        Date date = format.parse(data);
        return date;
    }

    // заполнение Map с командами
    public static Map<String, CommandEnum> getFullCommandMap() {
        Map<String, CommandEnum> commandMap = new HashMap<>();
        for (CommandEnum command : CommandEnum.values()) {
            commandMap.put(command.getName(), command);
        }
        return commandMap;
    }

}

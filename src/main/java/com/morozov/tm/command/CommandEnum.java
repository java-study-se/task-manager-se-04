package com.morozov.tm.command;

public enum CommandEnum {
    HELP("help", "Show all command"),
    PROJECT_CLEAR("project-clear", "Remove all projects"),
    PROJECT_CREATE("project-create", "Create project"),
    PROJECT_LIST("project-list", "Show all projects"),
    PROJECT_REMOVE("project-remove", "Remove selected project"),
    PROJECT_UPDATE("project-update", "Update selected project"),
    TASK_CLEAR("task-clear", "Remove all tasks"),
    TASK_LIST("task-list", "Show all tasks"),
    TASK_CREATE("task-create", "Create task"),
    TASK_UPDATE("task-update", "Update selected task"),
    TASK_REMOVE("task-remove", "Remove selected task"),
    TASK_LIST_BY_PROJECT_ID("task-list-id", "Show all task by selected project"),
    EXIT("exit", "Exit from programm");

    private String name;
    private String description;

    CommandEnum(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
